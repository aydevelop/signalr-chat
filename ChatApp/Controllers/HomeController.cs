﻿using ChatApp.Database;
using ChatApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace ChatApp.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private AppDbContext _ctx;
        public HomeController(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public IActionResult Index()
        {
            string id = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            List<Chat> chats = _ctx.ChatUsers
                .Include(x => x.Chat)
                .Where(x => x.UserId == id)
                .Select(x => x.Chat)
                .ToList();

            return View(chats);
        }

        public IActionResult Find()
        {
            var users = _ctx.Users.Where(q => q.Id != HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value).ToList();
            return View(users);
        }

        public async Task<IActionResult> CreatePrivateRoom(string userId)
        {
            var chat = new Chat
            {
                Type = ChatType.Private,
                Name = "Private with " + userId

            };

            chat.Users.Add(new ChatUser
            {
                UserId = userId
            });

            chat.Users.Add(new ChatUser
            {
                UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value
            });

            _ctx.Chats.Add(chat);
            await _ctx.SaveChangesAsync();

            return RedirectToAction("Chat", new { id = chat.Id });
        }

        public IActionResult Chat(int id)
        {
            var chat = _ctx.Chats
                .Include(x => x.Messages)
                .FirstOrDefault(x => x.Id == id);
            return View(chat);
        }

        [HttpPost]
        public async Task<IActionResult> CreateRoom(string name)
        {
            var chat = new Chat
            {
                Name = name,
                Type = ChatType.Room
            };

            chat.Users.Add(new ChatUser
            {
                UserId = (User.FindFirst(ClaimTypes.NameIdentifier).Value),
                Role = UserRole.Admin,
                Chat = chat
            });

            _ctx.Chats.Add(chat);
            await _ctx.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> CreateMsg(int chatId, string message)
        {
            var Message = new Message
            {
                ChatId = chatId,
                Text = message,
                Name = User.Identity.Name,
                Timestamp = DateTime.Now
            };

            _ctx.Messages.Add(Message);
            await _ctx.SaveChangesAsync();

            return RedirectToAction("Chat", new { id = chatId });
        }

        [HttpGet]
        public async Task<IActionResult> JoinRoom(int id)
        {
            var chatUser = new ChatUser
            {
                ChatId = id,
                UserId = (User.FindFirst(ClaimTypes.NameIdentifier).Value),
                Role = UserRole.Member,
            };

            _ctx.ChatUsers.Add(chatUser);
            await _ctx.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        public IActionResult Private()
        {
            var chats = _ctx.Chats.Include(q => q.Users).ThenInclude(q => q.User)
                .Where(q => q.Type == ChatType.Private
                && q.Users.Any(q => q.UserId == User.FindFirst(ClaimTypes.NameIdentifier).Value)).ToList();

            return View(chats);
        }
    }
}


