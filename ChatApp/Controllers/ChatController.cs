﻿using ChatApp.Database;
using ChatApp.Hubs;
using ChatApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Threading.Tasks;

namespace ChatApp.Controllers
{
    [Authorize]
    [Route("[controller]")]
    public class ChatController : Controller
    {
        private IHubContext<ChatHub> _chat;
        public ChatController(IHubContext<ChatHub> chat)
        {
            _chat = chat;
        }

        [HttpPost("[action]/{connectionId}/{roomName}")]
        public async Task<IActionResult> JoinRoom(string connectionId, string roomName)
        {
            Console.WriteLine("Joined test " + connectionId + " + " + roomName);
            await _chat.Groups.AddToGroupAsync(connectionId, roomName);
            await _chat.Clients.Group(roomName).SendAsync("JoinRoom", "Join to room: " + roomName);
            //await _chat.Clients.User("ROOT").SendAsync("JoinRoom", "User to room: " + roomName);
            return Ok();
        }

        [HttpPost("[action]/{connectionId}/{roomName}")]
        public async Task<IActionResult> LeaveRoom(string connectionId, string roomName)
        {
            await _chat.Groups.RemoveFromGroupAsync(connectionId, roomName);
            return Ok();
        }

        [HttpPost("SendMessage")]
        public async Task<IActionResult> SendMessage([FromForm] int chatId, [FromForm] string message, [FromForm] string roomName, [FromServices] AppDbContext ctx)
        {
            var msg = new Message
            {
                ChatId = chatId,
                Text = message,
                Name = User.Identity.Name,
                Timestamp = DateTime.Now
            };

            ctx.Messages.Add(msg);
            await ctx.SaveChangesAsync();
            await _chat.Clients.Group(roomName).SendAsync("ReceiveMsg", msg);
            //await _chat.Clients.All.SendAsync("ReceiveMsg", message);

            return Ok();
        }
    }
}
