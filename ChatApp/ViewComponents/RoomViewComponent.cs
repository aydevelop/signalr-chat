﻿using ChatApp.Database;
using ChatApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;


namespace ChatApp.ViewComponents
{
    public class RoomViewComponent : ViewComponent
    {
        private AppDbContext _ctx;

        public RoomViewComponent(AppDbContext ctx)
        {
            _ctx = ctx;
        }

        public IViewComponentResult Invoke()
        {
            List<Chat> chats = new List<Chat>();

            if (HttpContext.User.Identity.IsAuthenticated)
            {
                string id = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
                chats = _ctx.ChatUsers
                    .Include(x => x.Chat)
                    .Where(x => x.UserId == id && x.Chat.Type == ChatType.Room)
                    .Select(x => x.Chat)
                    .ToList();

            }

            return View(chats);
        }
    }
}
