﻿using Microsoft.AspNetCore.SignalR;
using System;

namespace ChatApp.Hubs
{
    public class ChatHub : Hub
    {
        public string GetConnectionId()
        {
            Console.WriteLine("Log ConnectionId: " + Context.ConnectionId);
            return Context.ConnectionId;
        }
    }
}
