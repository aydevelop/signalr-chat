﻿

window.addEventListener('load', function () {

    var createRoomBtn = document.getElementById("create-room-btn")
    var closeRoomBtn = document.getElementById("close-room")
    var createRoomModal = document.getElementById("create-room-modal")


    createRoomBtn.addEventListener('click', function () {
        createRoomModal.classList.add('active')
    })

    closeRoomBtn.addEventListener('click', function () {
        createRoomModal.classList.remove('active')
    })


})